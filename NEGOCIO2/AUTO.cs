﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace NEGOCIO
{
    public class AUTO
    {

        public AUTO(int ID, string marc, string model, string pat, float prec, int an, int state)
        {
            Id = ID;
            marca = marc;
            modelo = model;
            patente = pat;
            precio = prec;
            ano = an;
            estado = state;
        }
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string marca;

        public string Marca
        {
            get { return marca; }
            set { marca = value; }
        }

        private string modelo;

        public string Modelo
        {
            get { return modelo; }
            set { modelo = value; }
        }

        private string patente;

        public string Patente
        {
            get { return patente; }
            set { patente = value; }
        }

        private float precio;

        public float Precio
        {
            get { return precio; }
            set { precio = value; }
        }

        private int ano;

        public int Ano
        {
            get { return ano; }
            set { ano = value; }
        }

        private int estado;

        public int Estado
        {
            get { return estado; }
            set { estado = value; }
        }

        public static List<AUTO> ListarAutos()
        {
            List<AUTO> listaAutos = new List<AUTO>();

            ACCESO acceso = new ACCESO();
            acceso.Abrir();

            DataTable tabla = acceso.Leer("LeerAutos");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                AUTO auto = new AUTO(int.Parse(registro["id"].ToString()), registro["marca"].ToString(), registro["modelo"].ToString(), registro["patente"].ToString(), float.Parse(registro["precio"].ToString()), int.Parse(registro["año"].ToString()), int.Parse(registro["estado"].ToString()));
                listaAutos.Add(auto);
            }

            return listaAutos;
        }

        public static List<AUTO> ListarAutosDisponibles()
        {
            List<AUTO> listaAutos = new List<AUTO>();

            ACCESO acceso = new ACCESO();
            acceso.Abrir();

            DataTable tabla = acceso.Leer("LeerAutosDisponibles");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                AUTO auto = new AUTO(int.Parse(registro["id"].ToString()), registro["marca"].ToString(), registro["modelo"].ToString(), registro["patente"].ToString(), float.Parse(registro["precio"].ToString()), int.Parse(registro["año"].ToString()), int.Parse(registro["estado"].ToString()));
                listaAutos.Add(auto);
            }

            return listaAutos;
        }

        public static string IngresarAuto(string marca, string modelo, int ano, float precio, string patente)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();
            string resultado;

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("marca", marca));
            paratros.Add(acceso.CreaParametro("modelo", modelo));
            paratros.Add(acceso.CreaParametro("patente", patente));
            paratros.Add(acceso.CreaParametro("año", ano));
            paratros.Add(acceso.CreaParametro("precio", precio));

            resultado = acceso.Escribir("InsertarAuto", paratros);

            acceso.Cerrar();

            return resultado;
        }

        public static string Vender(AUTO auto, CLIENTE cliente)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();
            string resultado;

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("patente", auto.Patente));
            paratros.Add(acceso.CreaParametro("dni", cliente.Dni));

            resultado = acceso.Escribir("VenderAuto", paratros);

            acceso.Cerrar();

            return resultado;
        }

        public static string EliminarAuto(string patente)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();
            string resultado;

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("patente", patente));

            resultado = acceso.Escribir("BajaAuto", paratros);

            acceso.Cerrar();

            return resultado;
        }

        public override string ToString()
        {
            return marca + " " + modelo + " " + ano.ToString();
        }
    }
}
