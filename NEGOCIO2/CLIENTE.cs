﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace NEGOCIO
{
    public class CLIENTE
    {
        public CLIENTE()
        {

        }
        public CLIENTE(int ID, string nom, string ape, int DNI, int state)
        {
            id = ID;
            nombre = nom;
            apellido = ape;
            dni = DNI;
            estado = state;
        }

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        private int dni;

        public int Dni
        {
            get { return dni; }
            set { dni = value; }
        }

        private int estado;

        public int Estado
        {
            get { return estado; }
            set { estado = value; }
        }

        public static List<CLIENTE> ListarClientes()
        {
            List<CLIENTE> listaAutos = new List<CLIENTE>();

            ACCESO acceso = new ACCESO();
            acceso.Abrir();

            DataTable tabla = acceso.Leer("LeerClientes");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                CLIENTE cliente = new CLIENTE(int.Parse(registro["id"].ToString()), registro["nombre"].ToString(), registro["apellido"].ToString(), int.Parse(registro["dni"].ToString()), int.Parse(registro["estado"].ToString()));
                listaAutos.Add(cliente);
            }

            return listaAutos;
        }

        public static List<CLIENTE> ListarClientesActivos()
        {
            List<CLIENTE> listaAutos = new List<CLIENTE>();

            ACCESO acceso = new ACCESO();
            acceso.Abrir();

            DataTable tabla = acceso.Leer("LeerClientesActivos");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                CLIENTE cliente = new CLIENTE(int.Parse(registro["id"].ToString()), registro["nombre"].ToString(), registro["apellido"].ToString(), int.Parse(registro["dni"].ToString()), int.Parse(registro["estado"].ToString()));
                listaAutos.Add(cliente);
            }

            return listaAutos;
        }

        public static string EliminarCliente(int dni)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();
            string resultado;

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("dni", dni));

            resultado = acceso.Escribir("BajaCliente", paratros);

            acceso.Cerrar();

            return resultado;
        }

        public static string IngresarCliente(string nombre, string apellido, int dni)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();
            string resultado;

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("nombre", nombre));
            paratros.Add(acceso.CreaParametro("apellido", apellido));
            paratros.Add(acceso.CreaParametro("dni", dni)); 

            resultado = acceso.Escribir("InsertarCliente", paratros);

            acceso.Cerrar();

            return resultado;
        }

        public override string ToString()
        {
            return nombre + " " + apellido + " " + dni.ToString();
        }
    }
}