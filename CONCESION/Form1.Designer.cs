﻿namespace CONCESION
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnMostrarACliente = new System.Windows.Forms.Button();
            this.btnMostrarECliente = new System.Windows.Forms.Button();
            this.btnMostrarAAuto = new System.Windows.Forms.Button();
            this.btnMostrarEVehiculo = new System.Windows.Forms.Button();
            this.btnMostrarVenta = new System.Windows.Forms.Button();
            this.gbAgregarCliente = new System.Windows.Forms.GroupBox();
            this.btnAgregarCliente = new System.Windows.Forms.Button();
            this.lblEstadoACliente = new System.Windows.Forms.Label();
            this.txtDni = new System.Windows.Forms.TextBox();
            this.txtApellido = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.gbAgregarAuto = new System.Windows.Forms.GroupBox();
            this.txtPrecio = new System.Windows.Forms.TextBox();
            this.txtPatente = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnAgregarAuto = new System.Windows.Forms.Button();
            this.lblEstadoAAuto = new System.Windows.Forms.Label();
            this.txtAno = new System.Windows.Forms.TextBox();
            this.txtModelo = new System.Windows.Forms.TextBox();
            this.txtMarca = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Grilla = new System.Windows.Forms.DataGridView();
            this.gbVender = new System.Windows.Forms.GroupBox();
            this.cbAuto = new System.Windows.Forms.ComboBox();
            this.cbCliente = new System.Windows.Forms.ComboBox();
            this.btnVender = new System.Windows.Forms.Button();
            this.lblEstadoAVenta = new System.Windows.Forms.Label();
            this.lbAuto = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.gbEliminarAuto = new System.Windows.Forms.GroupBox();
            this.cbAutoEliminar = new System.Windows.Forms.ComboBox();
            this.btnEliminarAuto = new System.Windows.Forms.Button();
            this.lblEliminarAuto = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.gbEliminarCliente = new System.Windows.Forms.GroupBox();
            this.cbEliminarCliente = new System.Windows.Forms.ComboBox();
            this.btnEliminarCliente = new System.Windows.Forms.Button();
            this.lblEliminarCliente = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.gbAgregarCliente.SuspendLayout();
            this.gbAgregarAuto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grilla)).BeginInit();
            this.gbVender.SuspendLayout();
            this.gbEliminarAuto.SuspendLayout();
            this.gbEliminarCliente.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnMostrarACliente
            // 
            this.btnMostrarACliente.Location = new System.Drawing.Point(9, 10);
            this.btnMostrarACliente.Margin = new System.Windows.Forms.Padding(2);
            this.btnMostrarACliente.Name = "btnMostrarACliente";
            this.btnMostrarACliente.Size = new System.Drawing.Size(76, 62);
            this.btnMostrarACliente.TabIndex = 0;
            this.btnMostrarACliente.Text = "Agregar cliente";
            this.btnMostrarACliente.UseVisualStyleBackColor = true;
            this.btnMostrarACliente.Click += new System.EventHandler(this.btnMostrarACliente_Click);
            // 
            // btnMostrarECliente
            // 
            this.btnMostrarECliente.Location = new System.Drawing.Point(89, 10);
            this.btnMostrarECliente.Margin = new System.Windows.Forms.Padding(2);
            this.btnMostrarECliente.Name = "btnMostrarECliente";
            this.btnMostrarECliente.Size = new System.Drawing.Size(76, 62);
            this.btnMostrarECliente.TabIndex = 1;
            this.btnMostrarECliente.Text = "Eliminar cliente";
            this.btnMostrarECliente.UseVisualStyleBackColor = true;
            this.btnMostrarECliente.Click += new System.EventHandler(this.btnMostrarECliente_Click);
            // 
            // btnMostrarAAuto
            // 
            this.btnMostrarAAuto.Location = new System.Drawing.Point(170, 10);
            this.btnMostrarAAuto.Margin = new System.Windows.Forms.Padding(2);
            this.btnMostrarAAuto.Name = "btnMostrarAAuto";
            this.btnMostrarAAuto.Size = new System.Drawing.Size(76, 62);
            this.btnMostrarAAuto.TabIndex = 2;
            this.btnMostrarAAuto.Text = "Ingresar vehiculo";
            this.btnMostrarAAuto.UseVisualStyleBackColor = true;
            this.btnMostrarAAuto.Click += new System.EventHandler(this.btnMostrarAAuto_Click);
            // 
            // btnMostrarEVehiculo
            // 
            this.btnMostrarEVehiculo.Location = new System.Drawing.Point(250, 10);
            this.btnMostrarEVehiculo.Margin = new System.Windows.Forms.Padding(2);
            this.btnMostrarEVehiculo.Name = "btnMostrarEVehiculo";
            this.btnMostrarEVehiculo.Size = new System.Drawing.Size(76, 62);
            this.btnMostrarEVehiculo.TabIndex = 3;
            this.btnMostrarEVehiculo.Text = "Eliminar vehiculo";
            this.btnMostrarEVehiculo.UseVisualStyleBackColor = true;
            this.btnMostrarEVehiculo.Click += new System.EventHandler(this.btnMostrarEVehiculo_Click);
            // 
            // btnMostrarVenta
            // 
            this.btnMostrarVenta.Location = new System.Drawing.Point(330, 10);
            this.btnMostrarVenta.Margin = new System.Windows.Forms.Padding(2);
            this.btnMostrarVenta.Name = "btnMostrarVenta";
            this.btnMostrarVenta.Size = new System.Drawing.Size(76, 62);
            this.btnMostrarVenta.TabIndex = 4;
            this.btnMostrarVenta.Text = "Vender vehiculo";
            this.btnMostrarVenta.UseVisualStyleBackColor = true;
            this.btnMostrarVenta.Click += new System.EventHandler(this.btnMostrarVenta_Click);
            // 
            // gbAgregarCliente
            // 
            this.gbAgregarCliente.Controls.Add(this.btnAgregarCliente);
            this.gbAgregarCliente.Controls.Add(this.lblEstadoACliente);
            this.gbAgregarCliente.Controls.Add(this.txtDni);
            this.gbAgregarCliente.Controls.Add(this.txtApellido);
            this.gbAgregarCliente.Controls.Add(this.txtNombre);
            this.gbAgregarCliente.Controls.Add(this.label1);
            this.gbAgregarCliente.Controls.Add(this.label2);
            this.gbAgregarCliente.Controls.Add(this.label3);
            this.gbAgregarCliente.Location = new System.Drawing.Point(10, 77);
            this.gbAgregarCliente.Margin = new System.Windows.Forms.Padding(2);
            this.gbAgregarCliente.Name = "gbAgregarCliente";
            this.gbAgregarCliente.Padding = new System.Windows.Forms.Padding(2);
            this.gbAgregarCliente.Size = new System.Drawing.Size(279, 126);
            this.gbAgregarCliente.TabIndex = 5;
            this.gbAgregarCliente.TabStop = false;
            this.gbAgregarCliente.Text = "Agregar cliente";
            // 
            // btnAgregarCliente
            // 
            this.btnAgregarCliente.Location = new System.Drawing.Point(196, 22);
            this.btnAgregarCliente.Margin = new System.Windows.Forms.Padding(2);
            this.btnAgregarCliente.Name = "btnAgregarCliente";
            this.btnAgregarCliente.Size = new System.Drawing.Size(76, 62);
            this.btnAgregarCliente.TabIndex = 11;
            this.btnAgregarCliente.Text = "Agregar cliente";
            this.btnAgregarCliente.UseVisualStyleBackColor = true;
            this.btnAgregarCliente.Click += new System.EventHandler(this.btnAgregarCliente_Click);
            // 
            // lblEstadoACliente
            // 
            this.lblEstadoACliente.AutoSize = true;
            this.lblEstadoACliente.Location = new System.Drawing.Point(4, 96);
            this.lblEstadoACliente.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEstadoACliente.Name = "lblEstadoACliente";
            this.lblEstadoACliente.Size = new System.Drawing.Size(57, 13);
            this.lblEstadoACliente.TabIndex = 9;
            this.lblEstadoACliente.Text = "Sin estado";
            // 
            // txtDni
            // 
            this.txtDni.Location = new System.Drawing.Point(52, 67);
            this.txtDni.Margin = new System.Windows.Forms.Padding(2);
            this.txtDni.Name = "txtDni";
            this.txtDni.Size = new System.Drawing.Size(140, 20);
            this.txtDni.TabIndex = 11;
            // 
            // txtApellido
            // 
            this.txtApellido.Location = new System.Drawing.Point(52, 45);
            this.txtApellido.Margin = new System.Windows.Forms.Padding(2);
            this.txtApellido.Name = "txtApellido";
            this.txtApellido.Size = new System.Drawing.Size(140, 20);
            this.txtApellido.TabIndex = 10;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(52, 22);
            this.txtNombre.Margin = new System.Windows.Forms.Padding(2);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(140, 20);
            this.txtNombre.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 24);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Nombre";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 46);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Apellido";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 70);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "dni";
            // 
            // gbAgregarAuto
            // 
            this.gbAgregarAuto.Controls.Add(this.txtPrecio);
            this.gbAgregarAuto.Controls.Add(this.txtPatente);
            this.gbAgregarAuto.Controls.Add(this.label8);
            this.gbAgregarAuto.Controls.Add(this.label9);
            this.gbAgregarAuto.Controls.Add(this.btnAgregarAuto);
            this.gbAgregarAuto.Controls.Add(this.lblEstadoAAuto);
            this.gbAgregarAuto.Controls.Add(this.txtAno);
            this.gbAgregarAuto.Controls.Add(this.txtModelo);
            this.gbAgregarAuto.Controls.Add(this.txtMarca);
            this.gbAgregarAuto.Controls.Add(this.label5);
            this.gbAgregarAuto.Controls.Add(this.label6);
            this.gbAgregarAuto.Controls.Add(this.label7);
            this.gbAgregarAuto.Location = new System.Drawing.Point(293, 77);
            this.gbAgregarAuto.Margin = new System.Windows.Forms.Padding(2);
            this.gbAgregarAuto.Name = "gbAgregarAuto";
            this.gbAgregarAuto.Padding = new System.Windows.Forms.Padding(2);
            this.gbAgregarAuto.Size = new System.Drawing.Size(279, 169);
            this.gbAgregarAuto.TabIndex = 12;
            this.gbAgregarAuto.TabStop = false;
            this.gbAgregarAuto.Text = "Agregar auto";
            // 
            // txtPrecio
            // 
            this.txtPrecio.Location = new System.Drawing.Point(52, 111);
            this.txtPrecio.Margin = new System.Windows.Forms.Padding(2);
            this.txtPrecio.Name = "txtPrecio";
            this.txtPrecio.Size = new System.Drawing.Size(140, 20);
            this.txtPrecio.TabIndex = 15;
            // 
            // txtPatente
            // 
            this.txtPatente.Location = new System.Drawing.Point(52, 89);
            this.txtPatente.Margin = new System.Windows.Forms.Padding(2);
            this.txtPatente.Name = "txtPatente";
            this.txtPatente.Size = new System.Drawing.Size(140, 20);
            this.txtPatente.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 90);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Patente";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 114);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Precio";
            // 
            // btnAgregarAuto
            // 
            this.btnAgregarAuto.Location = new System.Drawing.Point(196, 22);
            this.btnAgregarAuto.Margin = new System.Windows.Forms.Padding(2);
            this.btnAgregarAuto.Name = "btnAgregarAuto";
            this.btnAgregarAuto.Size = new System.Drawing.Size(76, 62);
            this.btnAgregarAuto.TabIndex = 11;
            this.btnAgregarAuto.Text = "Agregar Auto";
            this.btnAgregarAuto.UseVisualStyleBackColor = true;
            this.btnAgregarAuto.Click += new System.EventHandler(this.btnAgregarAuto_Click);
            // 
            // lblEstadoAAuto
            // 
            this.lblEstadoAAuto.AutoSize = true;
            this.lblEstadoAAuto.Location = new System.Drawing.Point(4, 146);
            this.lblEstadoAAuto.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEstadoAAuto.Name = "lblEstadoAAuto";
            this.lblEstadoAAuto.Size = new System.Drawing.Size(57, 13);
            this.lblEstadoAAuto.TabIndex = 9;
            this.lblEstadoAAuto.Text = "Sin estado";
            // 
            // txtAno
            // 
            this.txtAno.Location = new System.Drawing.Point(52, 67);
            this.txtAno.Margin = new System.Windows.Forms.Padding(2);
            this.txtAno.Name = "txtAno";
            this.txtAno.Size = new System.Drawing.Size(140, 20);
            this.txtAno.TabIndex = 11;
            // 
            // txtModelo
            // 
            this.txtModelo.Location = new System.Drawing.Point(52, 45);
            this.txtModelo.Margin = new System.Windows.Forms.Padding(2);
            this.txtModelo.Name = "txtModelo";
            this.txtModelo.Size = new System.Drawing.Size(140, 20);
            this.txtModelo.TabIndex = 10;
            // 
            // txtMarca
            // 
            this.txtMarca.Location = new System.Drawing.Point(52, 22);
            this.txtMarca.Margin = new System.Windows.Forms.Padding(2);
            this.txtMarca.Name = "txtMarca";
            this.txtMarca.Size = new System.Drawing.Size(140, 20);
            this.txtMarca.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 24);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Marca";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 46);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Modelo";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 70);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(26, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Año";
            // 
            // Grilla
            // 
            this.Grilla.AllowUserToAddRows = false;
            this.Grilla.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Grilla.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.Grilla.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Grilla.Location = new System.Drawing.Point(9, 452);
            this.Grilla.Name = "Grilla";
            this.Grilla.ReadOnly = true;
            this.Grilla.Size = new System.Drawing.Size(563, 162);
            this.Grilla.TabIndex = 13;
            // 
            // gbVender
            // 
            this.gbVender.Controls.Add(this.cbAuto);
            this.gbVender.Controls.Add(this.cbCliente);
            this.gbVender.Controls.Add(this.btnVender);
            this.gbVender.Controls.Add(this.lblEstadoAVenta);
            this.gbVender.Controls.Add(this.lbAuto);
            this.gbVender.Controls.Add(this.label13);
            this.gbVender.Location = new System.Drawing.Point(9, 207);
            this.gbVender.Margin = new System.Windows.Forms.Padding(2);
            this.gbVender.Name = "gbVender";
            this.gbVender.Padding = new System.Windows.Forms.Padding(2);
            this.gbVender.Size = new System.Drawing.Size(279, 114);
            this.gbVender.TabIndex = 16;
            this.gbVender.TabStop = false;
            this.gbVender.Text = "Vender auto";
            // 
            // cbAuto
            // 
            this.cbAuto.FormattingEnabled = true;
            this.cbAuto.Location = new System.Drawing.Point(51, 22);
            this.cbAuto.Name = "cbAuto";
            this.cbAuto.Size = new System.Drawing.Size(140, 21);
            this.cbAuto.TabIndex = 13;
            // 
            // cbCliente
            // 
            this.cbCliente.FormattingEnabled = true;
            this.cbCliente.Location = new System.Drawing.Point(51, 46);
            this.cbCliente.Name = "cbCliente";
            this.cbCliente.Size = new System.Drawing.Size(140, 21);
            this.cbCliente.TabIndex = 12;
            // 
            // btnVender
            // 
            this.btnVender.Location = new System.Drawing.Point(196, 22);
            this.btnVender.Margin = new System.Windows.Forms.Padding(2);
            this.btnVender.Name = "btnVender";
            this.btnVender.Size = new System.Drawing.Size(76, 43);
            this.btnVender.TabIndex = 11;
            this.btnVender.Text = "Vender auto";
            this.btnVender.UseVisualStyleBackColor = true;
            this.btnVender.Click += new System.EventHandler(this.btnVender_Click);
            // 
            // lblEstadoAVenta
            // 
            this.lblEstadoAVenta.AutoSize = true;
            this.lblEstadoAVenta.Location = new System.Drawing.Point(4, 89);
            this.lblEstadoAVenta.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEstadoAVenta.Name = "lblEstadoAVenta";
            this.lblEstadoAVenta.Size = new System.Drawing.Size(57, 13);
            this.lblEstadoAVenta.TabIndex = 9;
            this.lblEstadoAVenta.Text = "Sin estado";
            // 
            // lbAuto
            // 
            this.lbAuto.AutoSize = true;
            this.lbAuto.Location = new System.Drawing.Point(4, 26);
            this.lbAuto.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbAuto.Name = "lbAuto";
            this.lbAuto.Size = new System.Drawing.Size(29, 13);
            this.lbAuto.TabIndex = 6;
            this.lbAuto.Text = "Auto";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(4, 51);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(39, 13);
            this.label13.TabIndex = 7;
            this.label13.Text = "Cliente";
            // 
            // gbEliminarAuto
            // 
            this.gbEliminarAuto.Controls.Add(this.cbAutoEliminar);
            this.gbEliminarAuto.Controls.Add(this.btnEliminarAuto);
            this.gbEliminarAuto.Controls.Add(this.lblEliminarAuto);
            this.gbEliminarAuto.Controls.Add(this.label10);
            this.gbEliminarAuto.Location = new System.Drawing.Point(293, 253);
            this.gbEliminarAuto.Margin = new System.Windows.Forms.Padding(2);
            this.gbEliminarAuto.Name = "gbEliminarAuto";
            this.gbEliminarAuto.Padding = new System.Windows.Forms.Padding(2);
            this.gbEliminarAuto.Size = new System.Drawing.Size(279, 76);
            this.gbEliminarAuto.TabIndex = 17;
            this.gbEliminarAuto.TabStop = false;
            this.gbEliminarAuto.Text = "Eliminar auto";
            // 
            // cbAutoEliminar
            // 
            this.cbAutoEliminar.FormattingEnabled = true;
            this.cbAutoEliminar.Location = new System.Drawing.Point(51, 22);
            this.cbAutoEliminar.Name = "cbAutoEliminar";
            this.cbAutoEliminar.Size = new System.Drawing.Size(140, 21);
            this.cbAutoEliminar.TabIndex = 13;
            // 
            // btnEliminarAuto
            // 
            this.btnEliminarAuto.Location = new System.Drawing.Point(196, 22);
            this.btnEliminarAuto.Margin = new System.Windows.Forms.Padding(2);
            this.btnEliminarAuto.Name = "btnEliminarAuto";
            this.btnEliminarAuto.Size = new System.Drawing.Size(76, 21);
            this.btnEliminarAuto.TabIndex = 11;
            this.btnEliminarAuto.Text = "Eliminar auto";
            this.btnEliminarAuto.UseVisualStyleBackColor = true;
            this.btnEliminarAuto.Click += new System.EventHandler(this.btnEliminarAuto_Click);
            // 
            // lblEliminarAuto
            // 
            this.lblEliminarAuto.AutoSize = true;
            this.lblEliminarAuto.Location = new System.Drawing.Point(4, 56);
            this.lblEliminarAuto.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEliminarAuto.Name = "lblEliminarAuto";
            this.lblEliminarAuto.Size = new System.Drawing.Size(57, 13);
            this.lblEliminarAuto.TabIndex = 9;
            this.lblEliminarAuto.Text = "Sin estado";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 26);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Auto";
            // 
            // gbEliminarCliente
            // 
            this.gbEliminarCliente.Controls.Add(this.cbEliminarCliente);
            this.gbEliminarCliente.Controls.Add(this.btnEliminarCliente);
            this.gbEliminarCliente.Controls.Add(this.lblEliminarCliente);
            this.gbEliminarCliente.Controls.Add(this.label15);
            this.gbEliminarCliente.Location = new System.Drawing.Point(9, 325);
            this.gbEliminarCliente.Margin = new System.Windows.Forms.Padding(2);
            this.gbEliminarCliente.Name = "gbEliminarCliente";
            this.gbEliminarCliente.Padding = new System.Windows.Forms.Padding(2);
            this.gbEliminarCliente.Size = new System.Drawing.Size(279, 94);
            this.gbEliminarCliente.TabIndex = 17;
            this.gbEliminarCliente.TabStop = false;
            this.gbEliminarCliente.Text = "Eliminar cliente";
            // 
            // cbEliminarCliente
            // 
            this.cbEliminarCliente.FormattingEnabled = true;
            this.cbEliminarCliente.Location = new System.Drawing.Point(51, 22);
            this.cbEliminarCliente.Name = "cbEliminarCliente";
            this.cbEliminarCliente.Size = new System.Drawing.Size(140, 21);
            this.cbEliminarCliente.TabIndex = 12;
            // 
            // btnEliminarCliente
            // 
            this.btnEliminarCliente.Location = new System.Drawing.Point(196, 22);
            this.btnEliminarCliente.Margin = new System.Windows.Forms.Padding(2);
            this.btnEliminarCliente.Name = "btnEliminarCliente";
            this.btnEliminarCliente.Size = new System.Drawing.Size(76, 37);
            this.btnEliminarCliente.TabIndex = 11;
            this.btnEliminarCliente.Text = "Eliminar cliente";
            this.btnEliminarCliente.UseVisualStyleBackColor = true;
            this.btnEliminarCliente.Click += new System.EventHandler(this.btnEliminarCliente_Click);
            // 
            // lblEliminarCliente
            // 
            this.lblEliminarCliente.AutoSize = true;
            this.lblEliminarCliente.Location = new System.Drawing.Point(4, 68);
            this.lblEliminarCliente.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEliminarCliente.Name = "lblEliminarCliente";
            this.lblEliminarCliente.Size = new System.Drawing.Size(57, 13);
            this.lblEliminarCliente.TabIndex = 9;
            this.lblEliminarCliente.Text = "Sin estado";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(4, 27);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(39, 13);
            this.label15.TabIndex = 7;
            this.label15.Text = "Cliente";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 626);
            this.Controls.Add(this.gbEliminarCliente);
            this.Controls.Add(this.gbEliminarAuto);
            this.Controls.Add(this.gbVender);
            this.Controls.Add(this.Grilla);
            this.Controls.Add(this.gbAgregarAuto);
            this.Controls.Add(this.gbAgregarCliente);
            this.Controls.Add(this.btnMostrarVenta);
            this.Controls.Add(this.btnMostrarEVehiculo);
            this.Controls.Add(this.btnMostrarAAuto);
            this.Controls.Add(this.btnMostrarECliente);
            this.Controls.Add(this.btnMostrarACliente);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.gbAgregarCliente.ResumeLayout(false);
            this.gbAgregarCliente.PerformLayout();
            this.gbAgregarAuto.ResumeLayout(false);
            this.gbAgregarAuto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grilla)).EndInit();
            this.gbVender.ResumeLayout(false);
            this.gbVender.PerformLayout();
            this.gbEliminarAuto.ResumeLayout(false);
            this.gbEliminarAuto.PerformLayout();
            this.gbEliminarCliente.ResumeLayout(false);
            this.gbEliminarCliente.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnMostrarACliente;
        private System.Windows.Forms.Button btnMostrarECliente;
        private System.Windows.Forms.Button btnMostrarAAuto;
        private System.Windows.Forms.Button btnMostrarEVehiculo;
        private System.Windows.Forms.Button btnMostrarVenta;
        private System.Windows.Forms.GroupBox gbAgregarCliente;
        private System.Windows.Forms.Button btnAgregarCliente;
        private System.Windows.Forms.Label lblEstadoACliente;
        private System.Windows.Forms.TextBox txtDni;
        private System.Windows.Forms.TextBox txtApellido;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox gbAgregarAuto;
        private System.Windows.Forms.Button btnAgregarAuto;
        private System.Windows.Forms.Label lblEstadoAAuto;
        private System.Windows.Forms.TextBox txtAno;
        private System.Windows.Forms.TextBox txtModelo;
        private System.Windows.Forms.TextBox txtMarca;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPrecio;
        private System.Windows.Forms.TextBox txtPatente;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView Grilla;
        private System.Windows.Forms.GroupBox gbVender;
        private System.Windows.Forms.ComboBox cbAuto;
        private System.Windows.Forms.ComboBox cbCliente;
        private System.Windows.Forms.Button btnVender;
        private System.Windows.Forms.Label lblEstadoAVenta;
        private System.Windows.Forms.Label lbAuto;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox gbEliminarAuto;
        private System.Windows.Forms.ComboBox cbAutoEliminar;
        private System.Windows.Forms.Button btnEliminarAuto;
        private System.Windows.Forms.Label lblEliminarAuto;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox gbEliminarCliente;
        private System.Windows.Forms.ComboBox cbEliminarCliente;
        private System.Windows.Forms.Button btnEliminarCliente;
        private System.Windows.Forms.Label lblEliminarCliente;
        private System.Windows.Forms.Label label15;
    }
}

