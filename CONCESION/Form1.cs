﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NEGOCIO;

namespace CONCESION
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
            Ocultar();
        }

        private void btnMostrarACliente_Click(object sender, EventArgs e)
        {
            Ocultar();
            gbAgregarCliente.Visible = true;
        }

        private void btnMostrarAAuto_Click(object sender, EventArgs e)
        {
            Ocultar();
            gbAgregarAuto.Visible = true;
        }

        private void btnMostrarVenta_Click(object sender, EventArgs e)
        {
            Ocultar();
            gbVender.Visible = true;
            EnlazarCBVenta();
        }

        private void btnMostrarEVehiculo_Click(object sender, EventArgs e)
        {
            Ocultar();
            EnlazarCBVenta();
            gbEliminarAuto.Visible = true;
        }

        private void btnMostrarECliente_Click(object sender, EventArgs e)
        {
            Ocultar();
            EnlazarCBVenta();
            gbEliminarCliente.Visible = true;
        }

        void EnlazarCBVenta()
        {
            cbAuto.DataSource = null;
            cbAuto.DataSource = AUTO.ListarAutosDisponibles();

            cbCliente.DataSource = null;
            cbCliente.DataSource = CLIENTE.ListarClientesActivos();

            cbAutoEliminar.DataSource = null;
            cbAutoEliminar.DataSource = AUTO.ListarAutosDisponibles();

            cbEliminarCliente.DataSource = null;
            cbEliminarCliente.DataSource = CLIENTE.ListarClientesActivos();
        }

        void Ocultar()
        {
            gbVender.Visible = false;
            gbAgregarCliente.Visible = false;
            gbAgregarAuto.Visible = false;
            Grilla.Visible = false;
            gbEliminarAuto.Visible = false;
            gbEliminarCliente.Visible = false;
        }

        void EnlazarClientesGrilla()
        {
            Grilla.Visible = true;
            Grilla.DataSource = null;
            Grilla.DataSource = CLIENTE.ListarClientes();
        }

        void EnlazarAutosGrilla()
        {
            Grilla.Visible = true;
            Grilla.DataSource = null;
            Grilla.DataSource = AUTO.ListarAutos();
        }

        private void btnAgregarCliente_Click(object sender, EventArgs e)
        {
            try
            {
                lblEstadoACliente.Text = (CLIENTE.IngresarCliente(txtNombre.Text, txtApellido.Text, int.Parse(txtDni.Text))).ToString();
                EnlazarClientesGrilla();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnAgregarAuto_Click(object sender, EventArgs e)
        {
            try
            {
                lblEstadoAAuto.Text = AUTO.IngresarAuto(txtMarca.Text,txtModelo.Text,int.Parse(txtAno.Text),float.Parse(txtPrecio.Text), txtPatente.Text.ToUpper());
                EnlazarAutosGrilla();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnVender_Click(object sender, EventArgs e)
        {
            try
            {
                lblEstadoAVenta.Text = AUTO.Vender((AUTO)cbAuto.SelectedValue, (CLIENTE)cbCliente.SelectedValue);
                EnlazarCBVenta();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEliminarAuto_Click(object sender, EventArgs e)
        {
            try
            {
                lblEliminarAuto.Text = AUTO.EliminarAuto(((AUTO)cbAutoEliminar.SelectedItem).Patente);
                EnlazarCBVenta();
            }
            catch (Exception ex)
            { 
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEliminarCliente_Click(object sender, EventArgs e)
        {
            try
            {
                lblEliminarCliente.Text = CLIENTE.EliminarCliente(((CLIENTE)cbEliminarCliente.SelectedItem).Dni);
                EnlazarCBVenta();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
