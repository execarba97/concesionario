USE [CONCESIONARIA]
GO
/****** Object:  Table [dbo].[AUTOS]    Script Date: 9/20/2020 8:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AUTOS](
	[id] [int] NOT NULL,
	[marca] [varchar](20) NULL,
	[modelo] [varchar](50) NULL,
	[año] [varchar](4) NULL,
	[precio] [float] NULL,
	[patente] [varchar](9) NULL,
	[estado] [int] NULL,
 CONSTRAINT [PK_AUTOS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CLIENTES]    Script Date: 9/20/2020 8:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CLIENTES](
	[id] [int] NOT NULL,
	[nombre] [varchar](50) NULL,
	[apellido] [varchar](50) NULL,
	[dni] [int] NULL,
	[estado] [int] NULL,
 CONSTRAINT [PK_CLIENTES] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VENTAS]    Script Date: 9/20/2020 8:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VENTAS](
	[id] [int] NOT NULL,
	[id_auto] [int] NOT NULL,
	[id_cliente] [int] NOT NULL,
	[fecha] [date] NULL,
 CONSTRAINT [PK_VENTAS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[BajaAuto]    Script Date: 9/20/2020 8:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[BajaAuto]
@patente varchar(9)
as
begin
	UPDATE [dbo].[AUTOS] SET estado = 0 WHERE patente = @patente
end
GO
/****** Object:  StoredProcedure [dbo].[BajaCliente]    Script Date: 9/20/2020 8:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[BajaCliente]
@dni int
as
begin
	UPDATE [dbo].[CLIENTES] SET estado = 0 WHERE dni = @dni
end

GO
/****** Object:  StoredProcedure [dbo].[InsertarAuto]    Script Date: 9/20/2020 8:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[InsertarAuto]
@patente varchar(9),
@año varchar(4),
@marca varchar(20),
@modelo varchar(50),
@precio float
as

begin
	declare @id int, @estado int
	set @estado = 1
	set @id = (SELECT ISNULL(MAX(id), 0)+1 FROM dbo.AUTOS)
	INSERT INTO [dbo].[AUTOS] (id,patente,año,marca,modelo,precio,estado) VALUES (@id,@patente,@año,@marca,@modelo,@precio,@estado)
end
GO
/****** Object:  StoredProcedure [dbo].[InsertarCliente]    Script Date: 9/20/2020 8:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[InsertarCliente]
@nombre varchar(50),
@apellido varchar(50),
@dni int
as
begin
	declare @id int, @estado int
	set @estado = 1
	set @id = (SELECT ISNULL(MAX(id), 0)+1 FROM dbo.CLIENTES)
	INSERT INTO [dbo].[CLIENTES] (id,nombre,apellido,dni,estado) VALUES (@id,@nombre,@apellido,@dni,@estado)
end
GO
/****** Object:  StoredProcedure [dbo].[LeerAutos]    Script Date: 9/20/2020 8:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[LeerAutos]
as
begin
	select id,marca,modelo,año,precio,patente,estado
	FROM [CONCESIONARIA].[dbo].[AUTOS]
end
GO
/****** Object:  StoredProcedure [dbo].[LeerAutosDisponibles]    Script Date: 9/20/2020 8:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[LeerAutosDisponibles]
as
begin
	select id,marca,modelo,año,precio,patente,estado
	FROM [CONCESIONARIA].[dbo].[AUTOS]
	where estado = 1
end
GO
/****** Object:  StoredProcedure [dbo].[LeerClientes]    Script Date: 9/20/2020 8:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[LeerClientes]
as
begin
	select id,nombre,apellido,dni,estado
	FROM [CONCESIONARIA].[dbo].[CLIENTES]
end
GO
/****** Object:  StoredProcedure [dbo].[LeerClientesActivos]    Script Date: 9/20/2020 8:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[LeerClientesActivos]
as
begin
	select id,nombre,apellido,dni,estado
	FROM [CONCESIONARIA].[dbo].[CLIENTES]
	where estado = 1
end
GO
/****** Object:  StoredProcedure [dbo].[VenderAuto]    Script Date: 9/20/2020 8:03:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[VenderAuto]
@patente varchar(9),
@dni int
as
begin
	declare @id int, @fecha date, @idAuto int, @idCliente int
	set @id = (SELECT ISNULL(MAX(id), 0)+1 FROM dbo.VENTAS)
	set @fecha = GETDATE()
	set @idAuto = (select id from dbo.AUTOS where patente = @patente)
	set @idCliente = (select id from dbo.CLIENTES where dni = @dni)

	INSERT INTO [dbo].[VENTAS] (id,id_auto,id_cliente,fecha) VALUES(@id,@idAuto,@idCliente,@fecha)
	UPDATE [dbo].[AUTOS] SET estado = 0 WHERE id = @idAuto
end

GO
